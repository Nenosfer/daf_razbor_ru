#!/usr/bin/python3
# -*- coding: utf-8 -*-

import tornado.web
from bson.objectid import ObjectId


class BaseHandler(tornado.web.RequestHandler):
    def initialize(self, **kwargs):
        super(BaseHandler, self).initialize(**kwargs)
        self.db = self.settings['db']

    def get_current_user(self):
        user_id = self.get_secure_cookie("usid")
        if not user_id: return None
        return self.db.users.find_one({'phone': str(user_id, 'utf-8')})


class Home(BaseHandler):
    def get(self):
        self.render("index.html")


class Otzivi(tornado.web.RequestHandler):
    def get(self):
        self.render("otzivi.html")


class Search(BaseHandler):
    def post(self):
        q = self.get_argument("link_name", None)
        addr = self.get_argument("address", None)
        if not q and not addr: raise tornado.web.HTTPError(403)
        if q: data = self.db.catalog.find({"name": {"$regex": q, "$options": "-i"}})
        if addr: data = self.db.catalog.find({"code": {"$regex": addr, "$options": "-i"}})
        self.render("search.html", data=data)


class TechCenter(BaseHandler):
    def get(self):
        self.render('tech.html')


class Partners(BaseHandler):
    def get(self):
        self.render('partners.html')


class Gallery(BaseHandler):
    def get(self):
        self.render("gallery.html")


class Evacuator(BaseHandler):
    def get(self):
        self.render("evacuator.html")


class TextPages(BaseHandler):
    def get(self, urls):
        page = self.db.pages.find_one({"url": urls})
        self.render("text.html", page=page)


class Sdelatzakaz(BaseHandler):
    def get(self):
        self.render("zakaz.html")


class Catalog(BaseHandler):
    def get(self, limit):
        li_t = 20
        sk = (int(limit) - 1) * li_t
        count = self.db.catalog.find({}).count()
        list_ = list(range(0, count, li_t))
        items = self.db.catalog.find({}).skip(sk).limit(20)
        self.render("catalog.html", count=count, items=items, paginator=list_, col=sk)


class Tovar(BaseHandler):
    def get(self, slug):
        tovar = self.db.catalog.find_one({"_id": ObjectId(slug)})
        self.render("tovar.html", tovar=tovar)