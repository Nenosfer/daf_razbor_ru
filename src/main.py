#!/usr/bin/python3
# -*- coding: utf-8 -*-
import tornado.httpserver
import tornado.ioloop
import tornado.web
from tornado.options import options
import pymongo
from settings import settings, mongo_address, MONGO_DB
from urls import url_patterns


class ShopApp(tornado.web.Application):
    def __init__(self, *args, **kwargs):
        mongo_addr = kwargs.get('mongo_addr', mongo_address)
        mongo_db = kwargs.get('mongo_db', MONGO_DB)
        db = pymongo.MongoClient(**mongo_addr)[mongo_db]
        super(ShopApp, self).__init__(url_patterns, db=db, *args, **dict(settings, **kwargs))


def main():
    app = ShopApp()
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    main()


