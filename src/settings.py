#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import tornado
import tornado.template
from tornado.options import define, options

# Make filepaths relative to settings.
location = lambda x: os.path.join(os.path.dirname(os.path.realpath(__file__)), x)

define("port", default=8877, help="run on the given port", type=int)
define("config", default=None, help="tornado config file")
define("debug", default=None, help="debug mode")

tornado.options.parse_command_line()

STATIC_ROOT = location('static')
TEMPLATE_ROOT = location('templates')

# Deployment Configuration
settings = {
    'debug': False,
    'template_path': TEMPLATE_ROOT,
    'static_path': STATIC_ROOT,
    'site_title': 'Запчасти Daf в Тольятти',
    'login_url': '/login',
    'xsrf_cookies': True,
    'cookie_secret': 'ewklh8y23rhbnfon23irubJertw54KGieuy8934ytbgjk320'
}

# Mongo settings


it_local = True

mongo_address = {
    'host': '192.168.199.24',
    'port': 27017,
}

# Mongo database name
MONGO_DB = "daf"

if options.config:
    tornado.options.parse_config_file(options.config)
