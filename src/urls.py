#!/usr/bin/python3
# -*- coding: utf-8 -*-
from tornado.web import url


from handlers import Home, Otzivi, TextPages, Search, Sdelatzakaz, Catalog, Tovar
from handlers import TechCenter
from handlers import Partners
from handlers import Evacuator
from handlers import Gallery

url_patterns = [
    url(r"/", Home, name="home"),
    url(r"/otzivi", Otzivi, name="otzivi"),
    url(r"/sdelat-zakaz", Sdelatzakaz, name="sdelat-zakaz"),
    url(r"/search", Search, name="search"),

    url(r"/tech", TechCenter, name="tech-center"),
    url(r"/partner", Partners, name="partner"),
    url(r"/evacuator", Evacuator, name="evacuator"),
    url(r"/gallery", Gallery, name="gallery"),

    url(r"/catalog/([0-9]*?)", Catalog, name="catalog"),
    url(r"/catalog/([A-Za-z0-9\-]*?)", Tovar, name="tovar"),
    url(r"/([A-Za-z0-9\-]*?)", TextPages, name="textpages"),
]
